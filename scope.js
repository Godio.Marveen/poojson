scope1.js :

     var x = "hello";

     function l(){
         console.log(x);
     }
     l();
       // Que donne "node scope1.js"? : ça donne hello
_________________________________________________________

       scope2.js :

     var x = "hello";

     function l(){
         x = "bye";
         console.log(x);
     }

     l();

     // Que donne "node scope2.js"? ça donne bye.

     _________________________________________________________

     scope3.js :
     
     var x = "hello";

     function l(){
         x = "bye";
     }

     console.log(x);
     l();
     console.log(x);

     // Que donne "node scope3.js"?ça donne hello puis bye.
